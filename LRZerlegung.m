function A = LRZerlegung(A)

n = size(A, 1); 

for i = 1:n-1
    
   if (A(i, i) == 0) 
      error('LRZ:Nulldivision','Keine LR-Zerlegung m�glich');
   end;
   
   A(i+1:n ,i) = -1*A(i+1:n, i) / A(i, i);
   A(i+1:n, i+1:n) = A(i+1:n, i+1:n) + A(i+1:n, i) * A(i, i+1:n); 
   
end;



