function [ M ] = MatrixMultiplikation( A, B )

    [zA,sA] = size(A);
    sB = size(B,2);

    M = zeros(zA,sB);

    for i = 1:zA
         for j = 1:sB
             for k = 1:sA
                 M(i,j) = M(i,j) + A(i,k) * B(k,j);
             end
         end
    end

end

