classdef Matrizen < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        MainUIFigure         matlab.ui.Figure
        MultiplikationPanel  matlab.ui.container.Panel
        MAPanel              matlab.ui.container.Panel
        MATable              matlab.ui.control.Table
        MAICButton           matlab.ui.control.Button
        MADCButton           matlab.ui.control.Button
        MADRButton           matlab.ui.control.Button
        MAIRButton           matlab.ui.control.Button
        MBPanel              matlab.ui.container.Panel
        MBTable              matlab.ui.control.Table
        MBICButton           matlab.ui.control.Button
        MBDCButton           matlab.ui.control.Button
        MBDRButton           matlab.ui.control.Button
        MBIRButton           matlab.ui.control.Button
        MRPanel              matlab.ui.container.Panel
        MRTable              matlab.ui.control.Table
        ErrorMessage         matlab.ui.control.Label
        MSymbol              matlab.ui.control.Label
        MRButton             matlab.ui.control.Button
        ResetButton          matlab.ui.control.Button
        LRZerlegungPanel     matlab.ui.container.Panel
        MZPanel              matlab.ui.container.Panel
        MZTable              matlab.ui.control.Table
        MZICButton           matlab.ui.control.Button
        MZDCButton           matlab.ui.control.Button
        MZDRButton           matlab.ui.control.Button
        MZIRButton           matlab.ui.control.Button
        MZRPanel             matlab.ui.container.Panel
        MZRTable             matlab.ui.control.Table
        MZRButton            matlab.ui.control.Button
        ZResetButton         matlab.ui.control.Button
        ZErrorMessage        matlab.ui.control.Label
        VerifyButton         matlab.ui.control.Button
    end


    properties (Access = private)
        
        DefaultMatrix = cell(3,3);
        ColumnWidth = 40;
                
    end

    methods (Access = private)
    
        function FormatMatricesTables(app)
         
            app.MATable.ColumnEditable = true;
            app.MATable.RowName = '';
            app.MBTable.ColumnEditable = true;
            app.MBTable.RowName = '';
            app.MRTable.ColumnEditable = false;
            app.MRTable.RowName = '';
            app.MZTable.ColumnEditable = true;
            app.MZTable.RowName = '';
            app.MZRTable.ColumnEditable = false;
            app.MZRTable.RowName = '';
            
        end
        
        function FormatMatricesTable(app, table)
         
            n = size(table.Data,2);
            nFormat = cell(1,n);
            nFormat(:) = {'numeric'};
            nWidth = cell(1,n);
            nWidth(:) = {app.ColumnWidth};
            table.ColumnWidth = nWidth;
            table.ColumnFormat = nFormat;
            
        end
        
        function IncreaseRows(app, table)
         
            table.Data = [table.Data; cell(1,size(table.Data,2))];
            
        end
        
        function IncreaseColumns(app, table)
            table.Data = [table.Data cell(size(table.Data,1),1)];
            FormatMatricesTable(app, table);
            
        end
        
        function DecreaseRows(app, table)
            
            rows = size(table.Data,1);
            if rows > 1
                table.Data = table.Data(1:rows-1,:);
            end;
                     
        end
        
        function DecreaseColumns(app, table)
            
            columns = size(table.Data,2);
            if columns > 1
                table.Data = table.Data(:,1:columns-1);
                FormatMatricesTable(app, table);
            end;           
        end
        
        
        function ResetMultiplication(app)
        
            app.MATable.Data = app.DefaultMatrix;
            FormatMatricesTable(app, app.MATable);
            app.MBTable.Data = app.DefaultMatrix;
            FormatMatricesTable(app, app.MBTable);
            ResetResult(app);
            
        end
        
        function ResetDecomposition(app)
            app.MZTable.Data = app.DefaultMatrix;
            FormatMatricesTable(app, app.MZTable);
            ResetZResult(app);
        end
        
        function ResetResult(app)
        
            app.MRTable.Data = {};
            app.ErrorMessage.Text = '';
            
        end
        
        function ResetZResult(app)
        
            app.MZRTable.Data = {};
            app.ZErrorMessage.Text = '';
            app.VerifyButton.Enable = 'off';
            
        end
        
    end


    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
        
            ResetMultiplication(app);
            ResetDecomposition(app);
            FormatMatricesTables(app);
            
        end

        % Button pushed function: MAIRButton
        function MAIRButtonPushed(app, event)
            IncreaseRows(app, app.MATable);
        end

        % Button pushed function: MADRButton
        function MADRButtonPushed(app, event)
            DecreaseRows(app, app.MATable);
        end

        % Button pushed function: MAICButton
        function MAICButtonPushed(app, event)
            IncreaseColumns(app, app.MATable);
            IncreaseRows(app, app.MBTable);
        end

        % Button pushed function: MADCButton
        function MADCButtonPushed(app, event)
            DecreaseColumns(app, app.MATable);
            DecreaseRows(app, app.MBTable);
        end

        % Button pushed function: MBICButton
        function MBICButtonPushed(app, event)
            IncreaseColumns(app, app.MBTable);    
        end

        % Button pushed function: MBDCButton
        function MBDCButtonPushed(app, event)
            DecreaseColumns(app, app.MBTable);        
        end

        % Button pushed function: MBDRButton
        function MBDRButtonPushed(app, event)
            DecreaseRows(app, app.MBTable); 
            DecreaseColumns(app, app.MATable);
        end

        % Button pushed function: MBIRButton
        function MBIRButtonPushed(app, event)
            IncreaseRows(app, app.MBTable);   
            IncreaseColumns(app, app.MATable);
        end

        % Button pushed function: MRButton
        function MRButtonPushed(app, event)
                      
            ResetResult(app);
            
            try
            
                A = cell2mat(app.MATable.Data);
                B = cell2mat(app.MBTable.Data);
                
                app.MRTable.Data = MatrixMultiplikation(A, B);
                FormatMatricesTable(app, app.MRTable);
            
            catch 
                
                app.ErrorMessage.Text = 'Eingabewerte �berpr�fen';
                
            end
           
          
        end

        % Button pushed function: ResetButton
        function ResetButtonPushed(app, event)
            ResetMultiplication(app);    
        end

        % Cell edit callback: MATable
        function MATableCellEdit(app, event)
             ResetResult(app);    
        end

        % Cell edit callback: MBTable
        function MBTableCellEdit(app, event)
             ResetResult(app);
        end

        % Button pushed function: MZICButton
        function MZICButtonPushed(app, event)
            IncreaseColumns(app, app.MZTable);    
        end

        % Button pushed function: MZDCButton
        function MZDCButtonPushed(app, event)
            DecreaseColumns(app, app.MZTable);     
        end

        % Button pushed function: MZDRButton
        function MZDRButtonPushed(app, event)
            DecreaseRows(app, app.MZTable); 
        end

        % Button pushed function: MZIRButton
        function MZIRButtonPushed(app, event)
            IncreaseRows(app, app.MZTable); 
        end

        % Button pushed function: ZResetButton
        function ZResetButtonPushed(app, event)
             ResetDecomposition(app);     
        end

        % Cell edit callback: MZRTable
        function MZRTableCellEdit(app, event)
            ResetZResult(app);    
        end

        % Button pushed function: MZRButton
        function MZRButtonPushed(app, event)
            
            ResetZResult(app); 
            
            try
            
                A = cell2mat(app.MZTable.Data);
                                
                app.MZRTable.Data = LRZerlegung(A);
                FormatMatricesTable(app, app.MZRTable);
                
                app.VerifyButton.Enable = 'on';
            
            catch e
                
                if (strcmp(e.identifier,'LRZ:Nulldivision'))
                    app.ZErrorMessage.Text = e.message;
                else
                    app.ZErrorMessage.Text = 'Eingabewerte �berpr�fen';
                end
                
                
                
            end    
        end

        % Button pushed function: VerifyButton
        function VerifyButtonPushed(app, event)
            ResetResult(app); 
            
            A = app.MZRTable.Data;
            
            L = -1 * tril(A,-1) + eye(size(A,1));
            R = triu(A);
            
            app.MATable.Data = L;
            app.MBTable.Data = R;
            
            app.MRTable.Data = MatrixMultiplikation(L, R);
            FormatMatricesTable(app, app.MRTable);
            
        end
    end

    % App initialization and construction
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create MainUIFigure
            app.MainUIFigure = uifigure;
            app.MainUIFigure.Color = [1 1 1];
            app.MainUIFigure.Position = [100 100 768 640];
            app.MainUIFigure.Name = 'Matrizen: Multiplikation und LR-Zerlegung';
            app.MainUIFigure.Resize = 'off';
            setAutoResize(app, app.MainUIFigure, true)

            % Create MultiplikationPanel
            app.MultiplikationPanel = uipanel(app.MainUIFigure);
            app.MultiplikationPanel.Title = ' Multiplikation';
            app.MultiplikationPanel.BackgroundColor = [1 1 1];
            app.MultiplikationPanel.FontSize = 16;
            app.MultiplikationPanel.Position = [1 321 768 320];

            % Create MAPanel
            app.MAPanel = uipanel(app.MultiplikationPanel);
            app.MAPanel.BorderType = 'none';
            app.MAPanel.BackgroundColor = [1 1 1];
            app.MAPanel.Position = [19 68 200 180];

            % Create MATable
            app.MATable = uitable(app.MAPanel);
            app.MATable.ColumnName = '';
            app.MATable.RowName = {};
            app.MATable.Position = [25 1 176 156];
            app.MATable.RowStriping = 'off';
            app.MATable.CellEditCallback = createCallbackFcn(app, @MATableCellEdit, true);

            % Create MAICButton
            app.MAICButton = uibutton(app.MAPanel, 'push');
            app.MAICButton.ButtonPushedFcn = createCallbackFcn(app, @MAICButtonPushed, true);
            app.MAICButton.Icon = 'sym-pfeil-rechts-gross.png';
            app.MAICButton.Position = [61 159 32 22];
            app.MAICButton.Text = '';

            % Create MADCButton
            app.MADCButton = uibutton(app.MAPanel, 'push');
            app.MADCButton.ButtonPushedFcn = createCallbackFcn(app, @MADCButtonPushed, true);
            app.MADCButton.Icon = 'sym-pfeil-links-gross.png';
            app.MADCButton.Position = [30 159 32 22];
            app.MADCButton.Text = '';

            % Create MADRButton
            app.MADRButton = uibutton(app.MAPanel, 'push');
            app.MADRButton.ButtonPushedFcn = createCallbackFcn(app, @MADRButtonPushed, true);
            app.MADRButton.Icon = 'sym-pfeil-hoch-gross.png';
            app.MADRButton.Position = [1 125 22 32];
            app.MADRButton.Text = '';

            % Create MAIRButton
            app.MAIRButton = uibutton(app.MAPanel, 'push');
            app.MAIRButton.ButtonPushedFcn = createCallbackFcn(app, @MAIRButtonPushed, true);
            app.MAIRButton.Icon = 'sym-pfeil-runter-gross.png';
            app.MAIRButton.Position = [1 94 22 32];
            app.MAIRButton.Text = '';

            % Create MBPanel
            app.MBPanel = uipanel(app.MultiplikationPanel);
            app.MBPanel.BorderType = 'none';
            app.MBPanel.BackgroundColor = [1 1 1];
            app.MBPanel.Position = [284 68 200 180];

            % Create MBTable
            app.MBTable = uitable(app.MBPanel);
            app.MBTable.ColumnName = {};
            app.MBTable.RowName = {};
            app.MBTable.Position = [25 1 176 156];
            app.MBTable.RowStriping = 'off';
            app.MBTable.CellEditCallback = createCallbackFcn(app, @MBTableCellEdit, true);

            % Create MBICButton
            app.MBICButton = uibutton(app.MBPanel, 'push');
            app.MBICButton.ButtonPushedFcn = createCallbackFcn(app, @MBICButtonPushed, true);
            app.MBICButton.Icon = 'sym-pfeil-rechts-gross.png';
            app.MBICButton.Position = [61 159 32 22];
            app.MBICButton.Text = '';

            % Create MBDCButton
            app.MBDCButton = uibutton(app.MBPanel, 'push');
            app.MBDCButton.ButtonPushedFcn = createCallbackFcn(app, @MBDCButtonPushed, true);
            app.MBDCButton.Icon = 'sym-pfeil-links-gross.png';
            app.MBDCButton.Position = [30 159 32 22];
            app.MBDCButton.Text = '';

            % Create MBDRButton
            app.MBDRButton = uibutton(app.MBPanel, 'push');
            app.MBDRButton.ButtonPushedFcn = createCallbackFcn(app, @MBDRButtonPushed, true);
            app.MBDRButton.Icon = 'sym-pfeil-hoch-gross.png';
            app.MBDRButton.Position = [1 125 22 32];
            app.MBDRButton.Text = '';

            % Create MBIRButton
            app.MBIRButton = uibutton(app.MBPanel, 'push');
            app.MBIRButton.ButtonPushedFcn = createCallbackFcn(app, @MBIRButtonPushed, true);
            app.MBIRButton.Icon = 'sym-pfeil-runter-gross.png';
            app.MBIRButton.Position = [1 94 22 32];
            app.MBIRButton.Text = '';

            % Create MRPanel
            app.MRPanel = uipanel(app.MultiplikationPanel);
            app.MRPanel.BorderType = 'none';
            app.MRPanel.BackgroundColor = [1 1 1];
            app.MRPanel.Position = [547 67 200 180];

            % Create MRTable
            app.MRTable = uitable(app.MRPanel);
            app.MRTable.ColumnName = {};
            app.MRTable.RowName = {};
            app.MRTable.Position = [25 1 176 156];
            app.MRTable.RowStriping = 'off';

            % Create ErrorMessage
            app.ErrorMessage = uilabel(app.MultiplikationPanel);
            app.ErrorMessage.HorizontalAlignment = 'right';
            app.ErrorMessage.FontColor = [0.5882 0 0];
            app.ErrorMessage.Position = [218 26 523 15];
            app.ErrorMessage.Text = '';

            % Create MSymbol
            app.MSymbol = uilabel(app.MultiplikationPanel);
            app.MSymbol.HorizontalAlignment = 'center';
            app.MSymbol.VerticalAlignment = 'center';
            app.MSymbol.FontSize = 36;
            app.MSymbol.FontWeight = 'bold';
            app.MSymbol.Position = [243 129 25 40];
            app.MSymbol.Text = '*';

            % Create MRButton
            app.MRButton = uibutton(app.MultiplikationPanel, 'push');
            app.MRButton.ButtonPushedFcn = createCallbackFcn(app, @MRButtonPushed, true);
            app.MRButton.FontSize = 36;
            app.MRButton.Position = [515 120 31 53];
            app.MRButton.Text = '=';

            % Create ResetButton
            app.ResetButton = uibutton(app.MultiplikationPanel, 'push');
            app.ResetButton.ButtonPushedFcn = createCallbackFcn(app, @ResetButtonPushed, true);
            app.ResetButton.Position = [43 19 100 22];
            app.ResetButton.Text = 'Zur�cksetzen';

            % Create LRZerlegungPanel
            app.LRZerlegungPanel = uipanel(app.MainUIFigure);
            app.LRZerlegungPanel.Title = ' LR-Zerlegung';
            app.LRZerlegungPanel.BackgroundColor = [1 1 1];
            app.LRZerlegungPanel.FontSize = 16;
            app.LRZerlegungPanel.Position = [1 1 768 321];

            % Create MZPanel
            app.MZPanel = uipanel(app.LRZerlegungPanel);
            app.MZPanel.BorderType = 'none';
            app.MZPanel.BackgroundColor = [1 1 1];
            app.MZPanel.Position = [18 69 200 180];

            % Create MZTable
            app.MZTable = uitable(app.MZPanel);
            app.MZTable.ColumnName = {};
            app.MZTable.RowName = {};
            app.MZTable.Position = [25 1 176 156];
            app.MZTable.RowStriping = 'off';

            % Create MZICButton
            app.MZICButton = uibutton(app.MZPanel, 'push');
            app.MZICButton.ButtonPushedFcn = createCallbackFcn(app, @MZICButtonPushed, true);
            app.MZICButton.Icon = 'sym-pfeil-rechts-gross.png';
            app.MZICButton.Position = [61 159 32 22];
            app.MZICButton.Text = '';

            % Create MZDCButton
            app.MZDCButton = uibutton(app.MZPanel, 'push');
            app.MZDCButton.ButtonPushedFcn = createCallbackFcn(app, @MZDCButtonPushed, true);
            app.MZDCButton.Icon = 'sym-pfeil-links-gross.png';
            app.MZDCButton.Position = [30 159 32 22];
            app.MZDCButton.Text = '';

            % Create MZDRButton
            app.MZDRButton = uibutton(app.MZPanel, 'push');
            app.MZDRButton.ButtonPushedFcn = createCallbackFcn(app, @MZDRButtonPushed, true);
            app.MZDRButton.Icon = 'sym-pfeil-hoch-gross.png';
            app.MZDRButton.Position = [1 125 22 32];
            app.MZDRButton.Text = '';

            % Create MZIRButton
            app.MZIRButton = uibutton(app.MZPanel, 'push');
            app.MZIRButton.ButtonPushedFcn = createCallbackFcn(app, @MZIRButtonPushed, true);
            app.MZIRButton.Icon = 'sym-pfeil-runter-gross.png';
            app.MZIRButton.Position = [1 94 22 32];
            app.MZIRButton.Text = '';

            % Create MZRPanel
            app.MZRPanel = uipanel(app.LRZerlegungPanel);
            app.MZRPanel.BorderType = 'none';
            app.MZRPanel.BackgroundColor = [1 1 1];
            app.MZRPanel.Position = [283 68 200 180];

            % Create MZRTable
            app.MZRTable = uitable(app.MZRPanel);
            app.MZRTable.ColumnName = {};
            app.MZRTable.RowName = {};
            app.MZRTable.Position = [25 1 176 156];
            app.MZRTable.RowStriping = 'off';
            app.MZRTable.CellEditCallback = createCallbackFcn(app, @MZRTableCellEdit, true);

            % Create MZRButton
            app.MZRButton = uibutton(app.LRZerlegungPanel, 'push');
            app.MZRButton.ButtonPushedFcn = createCallbackFcn(app, @MZRButtonPushed, true);
            app.MZRButton.Icon = 'sym-pfeil-rechts-gross.png';
            app.MZRButton.FontSize = 14;
            app.MZRButton.Position = [246.5 120 31 53];
            app.MZRButton.Text = '';

            % Create ZResetButton
            app.ZResetButton = uibutton(app.LRZerlegungPanel, 'push');
            app.ZResetButton.ButtonPushedFcn = createCallbackFcn(app, @ZResetButtonPushed, true);
            app.ZResetButton.Position = [43 24 100 22];
            app.ZResetButton.Text = 'Zur�cksetzen';

            % Create ZErrorMessage
            app.ZErrorMessage = uilabel(app.LRZerlegungPanel);
            app.ZErrorMessage.HorizontalAlignment = 'right';
            app.ZErrorMessage.FontColor = [0.5882 0 0];
            app.ZErrorMessage.Position = [217 27 523 15];
            app.ZErrorMessage.Text = '';

            % Create VerifyButton
            app.VerifyButton = uibutton(app.LRZerlegungPanel, 'push');
            app.VerifyButton.ButtonPushedFcn = createCallbackFcn(app, @VerifyButtonPushed, true);
            app.VerifyButton.Enable = 'off';
            app.VerifyButton.Icon = 'sym-pfeil-hoch-gross.png';
            app.VerifyButton.IconAlignment = 'right';
            app.VerifyButton.Position = [597 135 100 22];
            app.VerifyButton.Text = 'Probe';
        end
    end

    methods (Access = public)

        % Construct app
        function app = Matrizen()

            % Create and configure components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.MainUIFigure)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.MainUIFigure)
        end
    end
end